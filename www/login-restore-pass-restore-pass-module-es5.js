(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-restore-pass-restore-pass-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/login/restore-pass/restore-pass.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/login/restore-pass/restore-pass.page.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-buttons slot=\"start\">\n    <ion-back-button></ion-back-button>\n  </ion-buttons>\n</ion-header>\n  \n<ion-content class=\"background ion-text-center ion-padding\">\n  <ion-grid [ngStyle]=\"{'height': '100%'}\">\n      <ion-row  [ngStyle]=\"{'height': '50%'}\" justify-content-center align-items-center>\n        <img src=\"/assets/img/logo.png\" height=\"150\" width=\"150\" alt=\"\" class=\"logo\"/>\n        <ion-text>\n          <h1>Restablecer contraseña</h1>\n          <p class=\"ion-padding\">Necesitamos tu correo electronico para restablecer la contraseña.</p>\n        </ion-text>\n      </ion-row>\n\n      <ion-row [ngStyle]=\"{'height': '50%'}\">\n        <form [ngStyle]=\"{'height': '100%','position':'relative', 'width':'100%'}\" [formGroup]=\"restoreForm\" (ngSubmit)=\"restore()\">\n\n          <ion-item class=\"input-round\" [ngStyle]=\"{'position':'absolute','bottom':'35%','width':'100%'}\">\n            <img src=\"/assets/icon/email.png\" alt=\"\" class=\"icon\"/>\n            <ion-input class=\"ion-padding-start\" placeholder=\"Email\" type=\"mail\" formControlName=\"email\" ></ion-input>\n          </ion-item>\n        \n          <div [ngStyle]=\"{'position':'absolute','bottom':'10%','width':'100%'}\">\n            <div class=\"box-gradient\">\n              <ion-button expand=\"block\" fill=\"clear\" class=\"btn-home\" type=\"submit\">Enviar</ion-button>\n            </div>\n          </div>\n        </form>\n      </ion-row>\n      \n  </ion-grid>\n</ion-content>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/modal/modal.page.html":
/*!***********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/modal/modal.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content class=\"ion-padding ion-text-center\">\n  <img src=\"/assets/icon/{{img}}.png\" height=\"50\" width=\"50\" alt=\"\" class=\"logo\"/>\n  <h3>{{title}}</h3>\n  <p>{{message}}</p>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/login/restore-pass/restore-pass-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/login/restore-pass/restore-pass-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: RestorePassPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RestorePassPageRoutingModule", function() { return RestorePassPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _restore_pass_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./restore-pass.page */ "./src/app/login/restore-pass/restore-pass.page.ts");




var routes = [
    {
        path: '',
        component: _restore_pass_page__WEBPACK_IMPORTED_MODULE_3__["RestorePassPage"]
    }
];
var RestorePassPageRoutingModule = /** @class */ (function () {
    function RestorePassPageRoutingModule() {
    }
    RestorePassPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], RestorePassPageRoutingModule);
    return RestorePassPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/login/restore-pass/restore-pass.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/login/restore-pass/restore-pass.module.ts ***!
  \***********************************************************/
/*! exports provided: RestorePassPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RestorePassPageModule", function() { return RestorePassPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _restore_pass_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./restore-pass-routing.module */ "./src/app/login/restore-pass/restore-pass-routing.module.ts");
/* harmony import */ var _restore_pass_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./restore-pass.page */ "./src/app/login/restore-pass/restore-pass.page.ts");
/* harmony import */ var _pages_modal_modal_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../pages/modal/modal.page */ "./src/app/pages/modal/modal.page.ts");








var RestorePassPageModule = /** @class */ (function () {
    function RestorePassPageModule() {
    }
    RestorePassPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _restore_pass_routing_module__WEBPACK_IMPORTED_MODULE_5__["RestorePassPageRoutingModule"]
            ],
            declarations: [_restore_pass_page__WEBPACK_IMPORTED_MODULE_6__["RestorePassPage"], _pages_modal_modal_page__WEBPACK_IMPORTED_MODULE_7__["ModalPage"]],
            entryComponents: [_pages_modal_modal_page__WEBPACK_IMPORTED_MODULE_7__["ModalPage"]]
        })
    ], RestorePassPageModule);
    return RestorePassPageModule;
}());



/***/ }),

/***/ "./src/app/login/restore-pass/restore-pass.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/login/restore-pass/restore-pass.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".background {\n  --background: url(\"/assets/img/iniciar sesion bg.png\") 0 0/100% 100% no-repeat;\n}\n\nion-header {\n  position: initial;\n}\n\n.my-custom-modal-css .modal-wrapper {\n  height: 20%;\n  top: 80%;\n  position: absolute;\n  display: block;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2pvc2UvUHJvamVjdHMvdGF4aS9jb25kdWN0b3ItdGF4aS9zcmMvYXBwL2xvZ2luL3Jlc3RvcmUtcGFzcy9yZXN0b3JlLXBhc3MucGFnZS5zY3NzIiwic3JjL2FwcC9sb2dpbi9yZXN0b3JlLXBhc3MvcmVzdG9yZS1wYXNzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLDhFQUFBO0FDQ0o7O0FERUE7RUFDSSxpQkFBQTtBQ0NKOztBREVBO0VBQ0ksV0FBQTtFQUNBLFFBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL3Jlc3RvcmUtcGFzcy9yZXN0b3JlLXBhc3MucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJhY2tncm91bmR7XG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltZy9pbmljaWFyIHNlc2lvbiBiZy5wbmdcIikgMCAwLzEwMCUgMTAwJSBuby1yZXBlYXQ7XG59XG5cbmlvbi1oZWFkZXJ7XG4gICAgcG9zaXRpb246IGluaXRpYWw7XG59XG5cbi5teS1jdXN0b20tbW9kYWwtY3NzIC5tb2RhbC13cmFwcGVyIHtcbiAgICBoZWlnaHQ6IDIwJTtcbiAgICB0b3A6IDgwJTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7IFxuICAgIGRpc3BsYXk6IGJsb2NrOyAgXG4gfSIsIi5iYWNrZ3JvdW5kIHtcbiAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltZy9pbmljaWFyIHNlc2lvbiBiZy5wbmdcIikgMCAwLzEwMCUgMTAwJSBuby1yZXBlYXQ7XG59XG5cbmlvbi1oZWFkZXIge1xuICBwb3NpdGlvbjogaW5pdGlhbDtcbn1cblxuLm15LWN1c3RvbS1tb2RhbC1jc3MgLm1vZGFsLXdyYXBwZXIge1xuICBoZWlnaHQ6IDIwJTtcbiAgdG9wOiA4MCU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZGlzcGxheTogYmxvY2s7XG59Il19 */"

/***/ }),

/***/ "./src/app/login/restore-pass/restore-pass.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/login/restore-pass/restore-pass.page.ts ***!
  \*********************************************************/
/*! exports provided: RestorePassPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RestorePassPage", function() { return RestorePassPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _pages_modal_modal_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../pages/modal/modal.page */ "./src/app/pages/modal/modal.page.ts");





var RestorePassPage = /** @class */ (function () {
    function RestorePassPage(form, alertController, modalController) {
        this.form = form;
        this.alertController = alertController;
        this.modalController = modalController;
        this.restoreForm = form.group({
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email])],
        });
    }
    RestorePassPage.prototype.ngOnInit = function () {
    };
    RestorePassPage.prototype.restore = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(this.restoreForm.value);
                        return [4 /*yield*/, this.modalController.create({
                                component: _pages_modal_modal_page__WEBPACK_IMPORTED_MODULE_4__["ModalPage"],
                                cssClass: 'my-custom-modal-css',
                                componentProps: {
                                    'image': 'correo',
                                    'title': 'Restablecer contraseña',
                                    'content': 'Hemos enviado exitosamente un link a su correo para restablecer su contraseña'
                                }
                            })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    RestorePassPage.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] }
    ]; };
    RestorePassPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-restore-pass',
            template: __webpack_require__(/*! raw-loader!./restore-pass.page.html */ "./node_modules/raw-loader/index.js!./src/app/login/restore-pass/restore-pass.page.html"),
            styles: [__webpack_require__(/*! ./restore-pass.page.scss */ "./src/app/login/restore-pass/restore-pass.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]])
    ], RestorePassPage);
    return RestorePassPage;
}());



/***/ }),

/***/ "./src/app/pages/modal/modal.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/modal/modal.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21vZGFsL21vZGFsLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/modal/modal.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/modal/modal.page.ts ***!
  \*******************************************/
/*! exports provided: ModalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalPage", function() { return ModalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var ModalPage = /** @class */ (function () {
    function ModalPage(navParams) {
        this.img = navParams.get('image');
        this.title = navParams.get('title');
        this.message = navParams.get('content');
    }
    ModalPage.prototype.ngOnInit = function () {
    };
    ModalPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"] }
    ]; };
    ModalPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-modal',
            template: __webpack_require__(/*! raw-loader!./modal.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/modal/modal.page.html"),
            styles: [__webpack_require__(/*! ./modal.page.scss */ "./src/app/pages/modal/modal.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"]])
    ], ModalPage);
    return ModalPage;
}());



/***/ })

}]);
//# sourceMappingURL=login-restore-pass-restore-pass-module-es5.js.map