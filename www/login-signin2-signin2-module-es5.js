(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-signin2-signin2-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/login/signin2/signin2.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/login/signin2/signin2.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content class=\"background ion-padding\">\n\n  <form [formGroup]=\"singin2Form\" (ngSubmit)=\"signin2()\">\n    <ion-grid [ngStyle]=\"{'height':'100%'}\">\n\n      <ion-row [ngStyle]=\"{'width':'100%'}\">\n        <ion-col>\n          <span>Documento de identificación</span>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <ion-item class=\"input-round ion-margin-top\" [ngStyle]=\"{'width':'100%'}\">\n            <ion-label>Documento de identificación</ion-label>\n            <ion-select formControlName=\"typeDocument\">\n              <ion-select-option value=\"cedula\">INE/IFE</ion-select-option>\n              <ion-select-option value=\"pasaporte\">Pasaporte</ion-select-option>\n              <ion-select-option value=\"pasaporte\">Licencia</ion-select-option>\n            </ion-select>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <ion-item class=\"input-round ion-margin-top\" [ngStyle]=\"{'width':'100%'}\">\n            <img src=\"/assets/icon/email.png\" alt=\"\" class=\"icon\"/>\n            <ion-input class=\"ion-padding-start\" placeholder=\"Número de documento\" type=\"text\" formControlName=\"document\"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row class=\"ion-align-items-center ion-justify-content-center\">\n\n        <ion-col class=\"ion-float-left\" [ngStyle]=\"{'width':'100%'}\">\n          <div class=\"box-gradient\">\n            <ion-button fill=\"clear\" class=\"btn-home\" (click)=\"getPicture('identityFron')\">\n              <span class=\"ion-text-left\">Subir documento de identidad frontal</span>\n              <ion-icon slot=\"end\" name=\"cloud-upload\"></ion-icon>\n            </ion-button>\n          </div>\n        </ion-col>\n\n        <ion-col class=\"ion-float-right\" *ngIf=\"singin2Form.value.identityFron != ''\">\n          <ion-icon name=\"checkmark\"></ion-icon>\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row class=\"ion-align-items-center ion-justify-content-center\">\n\n        <ion-col class=\"ion-float-left\" [ngStyle]=\"{'width':'100%'}\">\n          <div class=\"box-gradient\">\n            <ion-button fill=\"clear\" expand=\"block\" class=\"btn-home\" (click)=\"getPicture('identityBack')\">\n              <span class=\"ion-text-left\">Subir documento de identidad posterior</span>\n              <ion-icon slot=\"end\" name=\"cloud-upload\"></ion-icon>\n            </ion-button>\n          </div>\n        </ion-col>\n        \n        <ion-col class=\"ion-float-right\" *ngIf=\"singin2Form.value.identityBack != ''\">\n          <ion-icon name=\"checkmark\"></ion-icon>\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row [ngStyle]=\"{'width':'100%'}\">\n        <ion-col>\n          <span class=\"ion-margin-top\">Licencia de conducir</span>\n        </ion-col>\n      </ion-row>\n\n      <ion-row class=\"ion-align-items-center ion-justify-content-center\">\n\n        <ion-col class=\"ion-float-left\" [ngStyle]=\"{'width':'100%'}\">\n          <div class=\"box-gradient\">\n            <ion-button fill=\"clear\" expand=\"block\" class=\"btn-home\" (click)=\"getPicture('licenseFron')\">\n              <span class=\"ion-text-left\">Subir licencia de conducir frontal</span>\n              <ion-icon slot=\"end\" name=\"cloud-upload\"></ion-icon>\n            </ion-button>\n          </div>\n        </ion-col>\n        \n        <ion-col class=\"ion-float-right\" *ngIf=\"singin2Form.value.licenseFron != ''\">\n          <ion-icon name=\"checkmark\"></ion-icon>\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row class=\"ion-align-items-center ion-justify-content-center\">\n\n        <ion-col class=\"ion-float-left\" [ngStyle]=\"{'width':'100%'}\">\n          <div class=\"box-gradient\">\n            <ion-button fill=\"clear\" expand=\"block\" class=\"btn-home\" (click)=\"getPicture('licenseBack')\">\n              <span class=\"ion-text-left\">Subir licencia de conducir posterior</span>\n              <ion-icon slot=\"end\" name=\"cloud-upload\"></ion-icon>\n            </ion-button>\n          </div>\n        </ion-col>\n        \n        <ion-col class=\"ion-float-right\" *ngIf=\"singin2Form.value.licenseBack != ''\">\n          <ion-icon name=\"checkmark\"></ion-icon>\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row [ngStyle]=\"{'width':'100%'}\">\n        <ion-col>\n          <span class=\"ion-margin-top\">Identificación de guia turistica</span>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <ion-item class=\"input-round ion-margin-top\" [ngStyle]=\"{'width':'100%'}\">\n            <ion-label>Selecciona los idiomas que hablas</ion-label>\n            <ion-select multiple formControlName = \"lenguages\">\n              <ion-select-option value=\"ingles\">Ingles</ion-select-option>\n              <ion-select-option value=\"español\">Español</ion-select-option>\n              <ion-select-option value=\"portugues\">Portugues</ion-select-option>\n              <ion-select-option value=\"italiano\">Italiano</ion-select-option>\n              <ion-select-option value=\"frances\">Frances</ion-select-option>\n              <ion-select-option value=\"aleman\">Aleman</ion-select-option>\n              <ion-select-option value=\"chino\">Chino</ion-select-option>\n            </ion-select>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row class=\"ion-align-items-center ion-justify-content-center\">\n\n        <ion-col class=\"ion-float-left\" [ngStyle]=\"{'width':'100%'}\">\n          <div class=\"box-gradient\">\n            <ion-button fill=\"clear\" expand=\"block\" class=\"btn-home\" (click)=\"getPicture('guideFront')\">\n              <span class=\"ion-text-left\">Subir identificación de guia turistica frontal</span>\n              <ion-icon slot=\"end\" name=\"cloud-upload\"></ion-icon>\n            </ion-button>\n          </div>\n        </ion-col>\n        \n        <ion-col class=\"ion-float-right\" *ngIf=\"singin2Form.value.guideFront != ''\">\n          <ion-icon name=\"checkmark\"></ion-icon>\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row class=\"ion-align-items-center ion-justify-content-center\">\n\n        <ion-col class=\"ion-float-left\" [ngStyle]=\"{'width':'100%'}\">\n          <div class=\"box-gradient\">\n            <ion-button fill=\"clear\" expand=\"block\" class=\"btn-home\" (click)=\"getPicture('guideBack')\">\n              <span class=\"ion-text-left\">Subir identificación de guia turistica posterior</span>\n              <ion-icon slot=\"end\" name=\"cloud-upload\"></ion-icon>\n            </ion-button>\n          </div>\n        </ion-col>\n        \n        <ion-col class=\"ion-float-right\" *ngIf=\"singin2Form.value.guideBack != ''\">\n          <ion-icon name=\"checkmark\"></ion-icon>\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <span [ngStyle]=\"{'font-size':'0.9rem', 'color':'#5f5c5a'}\">Esta informacion es opcional</span>\n        </ion-col>\n      </ion-row>\n\n  \n      <div class=\"ion-margin-top ion-text-center\">\n        <div class=\"box-gradient\">\n          <ion-button fill=\"clear\" expand=\"block\" class=\"btn-home\" type=\"submit\" [disabled]=\"!singin2Form.valid\">Continuar</ion-button>\n        </div>\n        <span>¿Ya tienes cuenta?<a class=\"link\" [routerLink]=\"['/login']\"> Inicia sesión</a></span>\n      </div>\n    </ion-grid>\n\n\n  </form>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/login/signin2/signin2-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/login/signin2/signin2-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: Signin2PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Signin2PageRoutingModule", function() { return Signin2PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _signin2_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./signin2.page */ "./src/app/login/signin2/signin2.page.ts");




var routes = [
    {
        path: '',
        component: _signin2_page__WEBPACK_IMPORTED_MODULE_3__["Signin2Page"]
    }
];
var Signin2PageRoutingModule = /** @class */ (function () {
    function Signin2PageRoutingModule() {
    }
    Signin2PageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], Signin2PageRoutingModule);
    return Signin2PageRoutingModule;
}());



/***/ }),

/***/ "./src/app/login/signin2/signin2.module.ts":
/*!*************************************************!*\
  !*** ./src/app/login/signin2/signin2.module.ts ***!
  \*************************************************/
/*! exports provided: Signin2PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Signin2PageModule", function() { return Signin2PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _signin2_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./signin2-routing.module */ "./src/app/login/signin2/signin2-routing.module.ts");
/* harmony import */ var _signin2_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./signin2.page */ "./src/app/login/signin2/signin2.page.ts");







var Signin2PageModule = /** @class */ (function () {
    function Signin2PageModule() {
    }
    Signin2PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _signin2_routing_module__WEBPACK_IMPORTED_MODULE_5__["Signin2PageRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
            ],
            declarations: [_signin2_page__WEBPACK_IMPORTED_MODULE_6__["Signin2Page"]]
        })
    ], Signin2PageModule);
    return Signin2PageModule;
}());



/***/ }),

/***/ "./src/app/login/signin2/signin2.page.scss":
/*!*************************************************!*\
  !*** ./src/app/login/signin2/signin2.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".background {\n  --background: url(\"/assets/img/bg.png\") 0 0/100% 100% no-repeat;\n}\n\nspan.ion-text-left {\n  margin-right: auto;\n}\n\n.btn-home {\n  font-size: 0.7rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2pvc2UvUHJvamVjdHMvdGF4aS9jb25kdWN0b3ItdGF4aS9zcmMvYXBwL2xvZ2luL3NpZ25pbjIvc2lnbmluMi5wYWdlLnNjc3MiLCJzcmMvYXBwL2xvZ2luL3NpZ25pbjIvc2lnbmluMi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSwrREFBQTtBQ0NKOztBREVBO0VBQ0ksa0JBQUE7QUNDSjs7QURFQTtFQUNJLGlCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9sb2dpbi9zaWduaW4yL3NpZ25pbjIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJhY2tncm91bmR7XG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltZy9iZy5wbmdcIikgMCAwLzEwMCUgMTAwJSBuby1yZXBlYXQ7XG59XG5cbnNwYW4uaW9uLXRleHQtbGVmdCB7XG4gICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xufVxuXG4uYnRuLWhvbWV7XG4gICAgZm9udC1zaXplOiAwLjdyZW07XG59IiwiLmJhY2tncm91bmQge1xuICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1nL2JnLnBuZ1wiKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcbn1cblxuc3Bhbi5pb24tdGV4dC1sZWZ0IHtcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xufVxuXG4uYnRuLWhvbWUge1xuICBmb250LXNpemU6IDAuN3JlbTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/login/signin2/signin2.page.ts":
/*!***********************************************!*\
  !*** ./src/app/login/signin2/signin2.page.ts ***!
  \***********************************************/
/*! exports provided: Signin2Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Signin2Page", function() { return Signin2Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/file-transfer/ngx */ "./node_modules/@ionic-native/file-transfer/ngx/index.js");







var Signin2Page = /** @class */ (function () {
    function Signin2Page(form, actionSheetController, camera, route, router, transfer, toastController) {
        var _this = this;
        this.form = form;
        this.actionSheetController = actionSheetController;
        this.camera = camera;
        this.route = route;
        this.router = router;
        this.transfer = transfer;
        this.toastController = toastController;
        // Datos de vista anterior
        this.route.queryParams.subscribe(function (params) {
            _this.idUser = _this.router.getCurrentNavigation().extras;
        });
        this.singin2Form = form.group({
            id_user: [''],
            typeDocument: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            document: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            identityFron: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            identityBack: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            licenseFron: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            licenseBack: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            lenguages: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            guideFront: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            guideBack: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
        });
    }
    Signin2Page.prototype.ngOnInit = function () {
    };
    Signin2Page.prototype.presentToast = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: message,
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    Signin2Page.prototype.takePicture = function (sourceType, formValueVariable) {
        var _this = this;
        var options = {
            quality: 100,
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(options).then(function (imageData) {
            _this.presentToast('Espere mientras se sube el archivo');
            var namefile = Date.now() + '.jpg';
            var options1 = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
            };
            var fileTransfer = _this.transfer.create();
            fileTransfer.upload(imageData, 'http://157.230.191.152/upload.php', options1).then(function (data) {
                _this.singin2Form.get(formValueVariable).setValue(namefile);
                _this.presentToast('Se subio con exito.');
            }, function (err) {
                alert('Error. Intente de nuevo');
            });
        }, function (err) {
            // Handle error
        });
    };
    Signin2Page.prototype.getPicture = function (formValueVariable) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var actionSheet;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(formValueVariable);
                        return [4 /*yield*/, this.actionSheetController.create({
                                header: 'Foto de pefil',
                                buttons: [{
                                        text: 'Galeria',
                                        icon: 'md-archive',
                                        handler: function () {
                                            _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY, formValueVariable);
                                        }
                                    }, {
                                        text: 'Camara',
                                        icon: 'md-camera',
                                        handler: function () {
                                            _this.takePicture(_this.camera.PictureSourceType.CAMERA, formValueVariable);
                                        }
                                    }, {
                                        text: 'Cancelar',
                                        icon: 'close',
                                        role: 'cancel',
                                        handler: function () {
                                            console.log('Cancel clicked');
                                        }
                                    }]
                            })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    Signin2Page.prototype.signin2 = function () {
        this.singin2Form.value.id_user = this.idUser;
        this.router.navigate(['signin3'], this.singin2Form.value);
    };
    Signin2Page.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"] },
        { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_4__["Camera"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
        { type: _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_6__["FileTransfer"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] }
    ]; };
    Signin2Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-signin2',
            template: __webpack_require__(/*! raw-loader!./signin2.page.html */ "./node_modules/raw-loader/index.js!./src/app/login/signin2/signin2.page.html"),
            styles: [__webpack_require__(/*! ./signin2.page.scss */ "./src/app/login/signin2/signin2.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_4__["Camera"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_6__["FileTransfer"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]])
    ], Signin2Page);
    return Signin2Page;
}());



/***/ })

}]);
//# sourceMappingURL=login-signin2-signin2-module-es5.js.map