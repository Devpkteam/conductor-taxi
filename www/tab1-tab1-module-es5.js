(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab1-tab1-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/tab1/tab1.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/tab1/tab1.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"6\">\n        <div class=\"ion-float-left\">\n          <img src=\"/assets/img/logo.png\" height=\"40\" width=\"40\" alt=\"\" class=\"logo\"/>      \n        </div>\n      </ion-col>\n      <ion-col size=\"6\">\n        <div class=\"ion-float-right\">\n          <ion-item lines=\"none\">\n            <ion-label>{{message}}</ion-label>\n            <ion-toggle color=\"primary\" (ionChange)=\"changeToggle($event)\"></ion-toggle>\n          </ion-item>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-header>\n\n<ion-content class=\"background\">\n\n    <ion-card class=\"card\">\n      <ion-card-header class=\"card-header\">\n          <div>Alameda Central</div>\n          <!-- <ion-card-title>Card Title</ion-card-title> -->\n      </ion-card-header>\n      <ion-card-content class=\"ion-margin-top\">\n        <p>Atracción turistica . Av Mecixo 5843 museo de arte en una hacienda histórica abierta hasta 6:00pm</p>\n        <div>\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"2\"></ion-col>\n              <ion-col size=\"5\">\n                <ion-button fill=\"clear\" class=\"btn-trip\">Tomar viaje</ion-button>\n              </ion-col>\n              <ion-col size=\"5\">\n                <ion-button fill=\"clear\" class=\"btn-trip\">Ver detalle</ion-button>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </div>\n      </ion-card-content>\n    </ion-card>\n\n    <ion-card class=\"card\">\n        <ion-card-header class=\"card-header-turist\">\n          <div class=\"ion-float-left\">\n            Alameda central\n          </div>\n          <div class=\"ion-float-right\">\n            Viaje turistico\n          </div>\n        </ion-card-header>\n        <ion-card-content class=\"ion-margin-top\">\n          <p>Atracción turistica . Av Mecixo 5843 museo de arte en una hacienda histórica abierta hasta 6:00pm</p>\n          <div>\n            <ion-grid>\n              <ion-row>\n                <ion-col size=\"2\"></ion-col>\n                <ion-col size=\"5\">\n                  <ion-button fill=\"clear\" class=\"btn-trip\">Tomar viaje</ion-button>\n                </ion-col>\n                <ion-col size=\"5\">\n                  <ion-button fill=\"clear\" class=\"btn-trip\">Ver detalle</ion-button>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n          </div>\n        </ion-card-content>\n    </ion-card>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/tab1/tab1.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/tab1/tab1.module.ts ***!
  \*******************************************/
/*! exports provided: Tab1PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1PageModule", function() { return Tab1PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab1.page */ "./src/app/pages/tab1/tab1.page.ts");







var Tab1PageModule = /** @class */ (function () {
    function Tab1PageModule() {
    }
    Tab1PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _tab1_page__WEBPACK_IMPORTED_MODULE_6__["Tab1Page"] }])
            ],
            declarations: [_tab1_page__WEBPACK_IMPORTED_MODULE_6__["Tab1Page"]]
        })
    ], Tab1PageModule);
    return Tab1PageModule;
}());



/***/ }),

/***/ "./src/app/pages/tab1/tab1.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/tab1/tab1.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".background {\n  --background: url(\"/assets/img/bg.png\") 0 0/100% 100% no-repeat;\n}\n\n.box-gradient {\n  margin: 0;\n  height: 100%;\n  border-left: 2px solid #a9da1f;\n  border-right: 2px solid #01acca;\n  background-size: 100% 2px;\n}\n\n.card {\n  --background: #f2f2f2 ;\n}\n\n.card-header {\n  border-bottom: 1px solid #cecece;\n  height: 40px;\n  font-size: 15px;\n  font-weight: 700;\n}\n\n.card-header-turist {\n  border-bottom: 1px solid #e4e4e4;\n  height: 40px;\n  --background: linear-gradient(90deg, #02b9c1, #83da22);\n  --color: white;\n  font-size: 15px;\n  font-weight: 700;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2pvc2UvUHJvamVjdHMvdGF4aS9jb25kdWN0b3ItdGF4aS9zcmMvYXBwL3BhZ2VzL3RhYjEvdGFiMS5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3RhYjEvdGFiMS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSwrREFBQTtBQ0NGOztBREVBO0VBQ0UsU0FBQTtFQUNBLFlBQUE7RUFDQSw4QkFBQTtFQUNBLCtCQUFBO0VBQ0EseUJBQUE7QUNDRjs7QURFQTtFQUNFLHNCQUFBO0FDQ0Y7O0FERUE7RUFDRSxnQ0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNDRjs7QURFQTtFQUNFLGdDQUFBO0VBQ0EsWUFBQTtFQUNBLHNEQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ0NGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdGFiMS90YWIxLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5iYWNrZ3JvdW5ke1xuICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1nL2JnLnBuZ1wiKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcbn1cblxuLmJveC1ncmFkaWVudHtcbiAgbWFyZ2luOiAwO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJvcmRlci1sZWZ0OiAycHggc29saWQgI2E5ZGExZjtcbiAgYm9yZGVyLXJpZ2h0OiAycHggc29saWQgIzAxYWNjYTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDJweDtcbn1cblxuLmNhcmR7XG4gIC0tYmFja2dyb3VuZDogI2YyZjJmMlxufVxuXG4uY2FyZC1oZWFkZXJ7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjY2VjZWNlO1xuICBoZWlnaHQ6IDQwcHg7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbn1cblxuLmNhcmQtaGVhZGVyLXR1cmlzdHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlNGU0ZTQ7XG4gIGhlaWdodDogNDBweDtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoOTBkZWcsICMwMmI5YzEsICM4M2RhMjIpO1xuICAtLWNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBmb250LXdlaWdodDogNzAwO1xufVxuIiwiLmJhY2tncm91bmQge1xuICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1nL2JnLnBuZ1wiKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcbn1cblxuLmJveC1ncmFkaWVudCB7XG4gIG1hcmdpbjogMDtcbiAgaGVpZ2h0OiAxMDAlO1xuICBib3JkZXItbGVmdDogMnB4IHNvbGlkICNhOWRhMWY7XG4gIGJvcmRlci1yaWdodDogMnB4IHNvbGlkICMwMWFjY2E7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAycHg7XG59XG5cbi5jYXJkIHtcbiAgLS1iYWNrZ3JvdW5kOiAjZjJmMmYyIDtcbn1cblxuLmNhcmQtaGVhZGVyIHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNjZWNlY2U7XG4gIGhlaWdodDogNDBweDtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBmb250LXdlaWdodDogNzAwO1xufVxuXG4uY2FyZC1oZWFkZXItdHVyaXN0IHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlNGU0ZTQ7XG4gIGhlaWdodDogNDBweDtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoOTBkZWcsICMwMmI5YzEsICM4M2RhMjIpO1xuICAtLWNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBmb250LXdlaWdodDogNzAwO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/tab1/tab1.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/tab1/tab1.page.ts ***!
  \*****************************************/
/*! exports provided: Tab1Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1Page", function() { return Tab1Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var Tab1Page = /** @class */ (function () {
    function Tab1Page() {
        this.message = 'No Disponible';
    }
    Tab1Page.prototype.changeToggle = function (event) {
        this.message = (event.detail.checked) ? 'Disponible' : 'No Disponible';
    };
    Tab1Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tab1',
            template: __webpack_require__(/*! raw-loader!./tab1.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/tab1/tab1.page.html"),
            styles: [__webpack_require__(/*! ./tab1.page.scss */ "./src/app/pages/tab1/tab1.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], Tab1Page);
    return Tab1Page;
}());



/***/ })

}]);
//# sourceMappingURL=tab1-tab1-module-es5.js.map