(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-signin-signin-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/login/signin/signin.page.html":
/*!*************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/login/signin/signin.page.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-buttons slot=\"start\">\n    <ion-back-button></ion-back-button>\n  </ion-buttons>\n</ion-header>\n\n<ion-content class=\"background ion-text-center\" padding>\n    <ion-text>\n      <h1>Crear Cuenta</h1>\n      <p [ngStyle]=\"{'font-size':'0.9em'}\">Añade tu foto de perfil</p>\n    </ion-text>\n\n    <div class=\"ion-text-center\">\n\n      <div [ngStyle]=\"{'position':'relative', 'width':'50%', 'display':'block', 'margin':'0 auto'}\" *ngIf=\"singinForm.value.photo == ''\">\n        <ion-button shape=\"round\" color=\"medium\" size=\"large\" fill=\"outline\" class=\"btn-camera\" (click)=\"getProfilePicture()\">\n          <img src=\"/assets/icon/camara.png\" alt=\"\">\n        </ion-button>\n        <img src=\"/assets/icon/mas.png\" alt=\"\" [ngStyle]=\"{'position':'absolute', 'bottom': '5%', 'right': '20%'}\">\n      </div>\n\n      <div *ngIf=\"singinForm.value.photo != ''\" [ngStyle]=\"{'position':'relative', 'width':'50%', 'display':'block', 'margin':'0 auto'}\">\n        <button shape=\"round\" class=\"btn-profile\" (click)=\"getProfilePicture()\">\n          <img src=\"http://157.230.191.152/uploads/{{singinForm.value.photo}}\" alt=\"\" [ngStyle]=\"{'border-radius':'50px', 'height':'100%'}\">\n        </button>\n        <img src=\"/assets/icon/mas.png\" alt=\"\" [ngStyle]=\"{'position':'absolute', 'bottom': '5%', 'right': '20%'}\">\n      </div>\n\n      <ion-text color=\"primary\" *ngIf=\"singinForm.value.photo != ''\" class=\"ion-no-margin\">\n        <h6>Imagen de perfil cargada</h6>\n      </ion-text>\n\n    </div>\n\n    <form [formGroup]=\"singinForm\" (ngSubmit)=\"signin()\" [ngStyle]=\"{'margin-top': '3em'}\">\n      <ion-item class=\"input-round\">\n        <img src=\"/assets/icon/email.png\" alt=\"\" class=\"icon\"/>\n        <ion-input class=\"ion-padding-start\" placeholder=\"Nombre y apellido\" type=\"text\" formControlName=\"name\" ></ion-input>\n      </ion-item>\n\n      <ion-text color=\"danger\" *ngIf=\"!singinForm.controls.name.valid && singinForm.controls.name.dirty\" class=\"ion-no-margin\">\n        <h6>El nombre es requerido, debe tener por lo menos cuatro (04) letras, solo letras.</h6>\n      </ion-text>\n    \n      <ion-item class=\"input-round ion-margin-top\">\n        <img src=\"/assets/icon/telefono.png\" alt=\"\" class=\"icon\"/>\n        <ion-input placeholder=\"Número de teléfono\" type=\"text\" formControlName=\"phone\"></ion-input>\n      </ion-item>\n\n      <ion-text color=\"danger\" *ngIf=\"!singinForm.controls.phone.valid && singinForm.controls.phone.dirty\" class=\"ion-no-margin\">\n        <h6>El teléfono debe tener por lo menos diez (10) numeros.</h6>\n      </ion-text>\n\n      <ion-item class=\"input-round ion-margin-top\">\n        <img src=\"/assets/icon/correo.png\" alt=\"\" class=\"icon\"/>\n        <ion-input placeholder=\"E-mail\" type=\"mail\" formControlName=\"email\"></ion-input>\n      </ion-item>\n\n      <ion-text color=\"danger\" *ngIf=\"!singinForm.controls.email.valid && singinForm.controls.email.dirty\" class=\"ion-no-margin\">\n        <h6>Debe ingresar un email valido.</h6>\n      </ion-text>\n\n      <ion-item class=\"input-round ion-margin-top\">\n        <img src=\"/assets/icon/contraseña.png\" alt=\"\" class=\"icon\"/>\n        <ion-input placeholder=\"Contraseña\" type=\"password\" formControlName=\"password\"></ion-input>\n      </ion-item>\n\n      <ion-text color=\"danger\" *ngIf=\"!singinForm.controls.password.valid && singinForm.controls.password.dirty\" class=\"ion-no-margin\">\n        <h6>La contraseña debe tener por lo menos ocho (08) caracteres.</h6>\n      </ion-text>\n\n      <ion-item class=\"input-round ion-margin-top\">\n        <img src=\"/assets/icon/contraseña.png\" alt=\"\" class=\"icon\"/>\n        <ion-input placeholder=\"Confirmar Contraseña\" type=\"password\" formControlName=\"repassword\"></ion-input>\n      </ion-item>\n\n      <ion-text color=\"danger\" *ngIf=\"!singinForm.controls.repassword.valid && singinForm.controls.repassword.dirty && singinForm.controls.repassword.hasError('pw_mismatch')\" class=\"ion-no-margin\">\n        <h6>Las contraseñas no coincide</h6>\n      </ion-text>\n\n      <div class=\"bottom-element ion-margin-top\">\n        <div class=\"box-gradient\">\n          <ion-button expand=\"block\" fill=\"clear\" class=\"btn-home\" type=\"submit\" [disabled]=\"!singinForm.valid\">Continuar</ion-button>\n        </div>\n        <span>¿Ya tienes cuenta?<a class=\"link\" [routerLink]=\"['/login']\"> Inicia sesión</a></span>\n      </div>\n    </form>\n</ion-content>\n\n"

/***/ }),

/***/ "./src/app/login/signin/signin-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/login/signin/signin-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: SigninPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SigninPageRoutingModule", function() { return SigninPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _signin_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./signin.page */ "./src/app/login/signin/signin.page.ts");




var routes = [
    {
        path: '',
        component: _signin_page__WEBPACK_IMPORTED_MODULE_3__["SigninPage"]
    }
];
var SigninPageRoutingModule = /** @class */ (function () {
    function SigninPageRoutingModule() {
    }
    SigninPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], SigninPageRoutingModule);
    return SigninPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/login/signin/signin.module.ts":
/*!***********************************************!*\
  !*** ./src/app/login/signin/signin.module.ts ***!
  \***********************************************/
/*! exports provided: SigninPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SigninPageModule", function() { return SigninPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _signin_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./signin-routing.module */ "./src/app/login/signin/signin-routing.module.ts");
/* harmony import */ var _signin_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./signin.page */ "./src/app/login/signin/signin.page.ts");







var SigninPageModule = /** @class */ (function () {
    function SigninPageModule() {
    }
    SigninPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _signin_routing_module__WEBPACK_IMPORTED_MODULE_5__["SigninPageRoutingModule"]
            ],
            declarations: [_signin_page__WEBPACK_IMPORTED_MODULE_6__["SigninPage"]]
        })
    ], SigninPageModule);
    return SigninPageModule;
}());



/***/ }),

/***/ "./src/app/login/signin/signin.page.scss":
/*!***********************************************!*\
  !*** ./src/app/login/signin/signin.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".background {\n  --background: url(\"/assets/img/bg.png\") 0 0/100% 100% no-repeat;\n}\n\nion-header {\n  position: initial;\n  background: #f6f9fb;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2pvc2UvUHJvamVjdHMvdGF4aS9jb25kdWN0b3ItdGF4aS9zcmMvYXBwL2xvZ2luL3NpZ25pbi9zaWduaW4ucGFnZS5zY3NzIiwic3JjL2FwcC9sb2dpbi9zaWduaW4vc2lnbmluLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLCtEQUFBO0FDQ0o7O0FERUE7RUFDSSxpQkFBQTtFQUNBLG1CQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9sb2dpbi9zaWduaW4vc2lnbmluLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5iYWNrZ3JvdW5ke1xuICAgIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWcvYmcucG5nXCIpIDAgMC8xMDAlIDEwMCUgbm8tcmVwZWF0O1xufVxuXG5pb24taGVhZGVye1xuICAgIHBvc2l0aW9uOiBpbml0aWFsO1xuICAgIGJhY2tncm91bmQ6ICNmNmY5ZmI7XG59IiwiLmJhY2tncm91bmQge1xuICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1nL2JnLnBuZ1wiKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcbn1cblxuaW9uLWhlYWRlciB7XG4gIHBvc2l0aW9uOiBpbml0aWFsO1xuICBiYWNrZ3JvdW5kOiAjZjZmOWZiO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/login/signin/signin.page.ts":
/*!*********************************************!*\
  !*** ./src/app/login/signin/signin.page.ts ***!
  \*********************************************/
/*! exports provided: SigninPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SigninPage", function() { return SigninPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _services_corek_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/corek.service */ "./src/app/services/corek.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/file-transfer/ngx */ "./node_modules/@ionic-native/file-transfer/ngx/index.js");








var SigninPage = /** @class */ (function () {
    function SigninPage(form, actionSheetController, loadingController, toastController, alertController, camera, _corek, router, transfer) {
        this.form = form;
        this.actionSheetController = actionSheetController;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.alertController = alertController;
        this.camera = camera;
        this._corek = _corek;
        this.router = router;
        this.transfer = transfer;
        this.singinForm = form.group({
            photo: ['',],
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("[a-zA-Z ]*")])],
            phone: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(10)])],
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email])],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(8)])],
            repassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        }, { validator: this.passwordsMatch });
    }
    SigninPage.prototype.ngOnInit = function () {
    };
    SigninPage.prototype.passwordsMatch = function (cg) {
        var pwd1 = cg.get('password').value;
        var pwd2 = cg.get('repassword').value;
        if ((pwd1 && pwd2) && pwd1 !== pwd2) {
            cg.controls['repassword'].setErrors({ "pw_mismatch": true });
            return { "pw_mismatch": true };
        }
        else {
            return null;
        }
    };
    SigninPage.prototype.takePicture = function (sourceType) {
        var _this = this;
        var options = {
            quality: 100,
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(options).then(function (imageData) {
            _this.presentToast('Espere mientras se sube el archivo');
            var namefile = Date.now() + '.jpg';
            var options1 = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
            };
            var fileTransfer = _this.transfer.create();
            fileTransfer.upload(imageData, 'http://157.230.191.152/upload.php', options1).then(function (data) {
                _this.singinForm.get('photo').setValue(namefile);
                _this.presentToast('Se subio con exito.');
            }, function (err) {
                alert('Error. Intente de nuevo');
            });
        }, function (err) {
            alert('error');
        });
    };
    SigninPage.prototype.getProfilePicture = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var actionSheet;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            header: 'Foto de pefil',
                            buttons: [{
                                    text: 'Galeria',
                                    icon: 'md-archive',
                                    handler: function () {
                                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                                    }
                                }, {
                                    text: 'Camara',
                                    icon: 'md-camera',
                                    handler: function () {
                                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                                    }
                                }, {
                                    text: 'Cancelar',
                                    icon: 'close',
                                    role: 'cancel',
                                    handler: function () {
                                        console.log('Cancel clicked');
                                    }
                                }]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    SigninPage.prototype.presentToast = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: message,
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    SigninPage.prototype.signin = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, conection;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingController.create({
                            duration: 15000,
                            message: 'Espere por favor...',
                            translucent: true,
                        })];
                    case 1:
                        loading = _a.sent();
                        loading.present();
                        conection = Date.now().toString + 'conection' + Math.random();
                        this._corek.ConnectCorekconfig(conection);
                        this._corek.socket.on(conection, function (response) {
                            var getUser = Date.now().toString() + 'get_user' + Math.random();
                            _this._corek.socket.emit('get_users', { 'condition': { 'user_email': _this.singinForm.value.email }, 'event': getUser });
                            _this._corek.socket.on(getUser, function (users) {
                                if (users.length > 0) {
                                    // Usuario ya registrado
                                    loading.dismiss();
                                    _this.presentToast('El email ya se encuentra registrado.');
                                }
                                else {
                                    // Insertar usuario
                                    var insertUser = Date.now().toString() + "insertUser" + Math.random();
                                    _this._corek.socket.emit('insert_user', { 'insert': {
                                            'user_login': _this.singinForm.value.email,
                                            'user_pass': _this.singinForm.value.password,
                                            'user_identification': 0,
                                            'user_email': _this.singinForm.value.email,
                                            'user_registered': Date.now(),
                                            'user_phone': _this.singinForm.value.phone,
                                            'user_status': 0,
                                            'user_photo': _this.singinForm.value.photo,
                                            'display_name': _this.singinForm.value.name,
                                        }, 'event': insertUser });
                                    _this._corek.socket.on(insertUser, function (response) {
                                        loading.dismiss();
                                        _this.presentToast('Registro Exitoso.');
                                        _this.router.navigate(['signin2'], response.insertId);
                                    });
                                }
                            });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    SigninPage.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
        { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_4__["Camera"] },
        { type: _services_corek_service__WEBPACK_IMPORTED_MODULE_5__["CorekService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
        { type: _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_7__["FileTransfer"] }
    ]; };
    SigninPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-signin',
            template: __webpack_require__(/*! raw-loader!./signin.page.html */ "./node_modules/raw-loader/index.js!./src/app/login/signin/signin.page.html"),
            styles: [__webpack_require__(/*! ./signin.page.scss */ "./src/app/login/signin/signin.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_4__["Camera"],
            _services_corek_service__WEBPACK_IMPORTED_MODULE_5__["CorekService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_7__["FileTransfer"]])
    ], SigninPage);
    return SigninPage;
}());



/***/ })

}]);
//# sourceMappingURL=login-signin-signin-module-es5.js.map