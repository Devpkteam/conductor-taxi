(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-signin3-signin3-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/login/signin3/signin3.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/login/signin3/signin3.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header translucent>\n  <ion-buttons slot=\"start\">\n    <ion-back-button></ion-back-button>\n  </ion-buttons>\n</ion-header>\n\n<ion-content class=\"background ion-padding\">\n  <form [formGroup]=\"singin3Form\" (ngSubmit)=\"signin3()\">\n\n    <ion-grid [ngStyle]=\"{'height':'100%'}\">\n\n      <ion-row [ngStyle]=\"{'width':'100%'}\">\n        <ion-col>\n          <span>Referencias personales</span>\n        </ion-col>\n      </ion-row>\n\n      <ion-row [ngStyle]=\"{'width':'100%'}\">\n        <ion-col>\n          <ion-item class=\"input-round ion-margin-top\">\n            <img src=\"/assets/icon/email.png\" alt=\"\" class=\"icon\"/>\n            <ion-input class=\"ion-padding-start\" placeholder=\"Nombre y apellido\" type=\"text\" formControlName=\"referenceName\"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row [ngStyle]=\"{'width':'100%'}\">\n        <ion-col>\n          <ion-item class=\"input-round ion-margin-top\">\n            <img src=\"/assets/icon/telefono.png\" alt=\"\" class=\"icon\"/>\n            <ion-input class=\"ion-padding-start\" placeholder=\"Número de teléfono\" type=\"text\" formControlName=\"referenceNumber\"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row [ngStyle]=\"{'width':'100%'}\">\n        <ion-col>\n          <ion-item class=\"input-round ion-margin-top\" [ngStyle]=\"{'margin-bottom':'40px'}\">\n            <img src=\"/assets/icon/email.png\" alt=\"\" class=\"icon\"/>\n            <ion-input class=\"ion-padding-start\" placeholder=\"Dirección\" type=\"text\" formControlName=\"referenceDirection\"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row [ngStyle]=\"{'width':'100%'}\">\n        <ion-col>\n          <ion-item class=\"input-round ion-margin-top\">\n            <img src=\"/assets/icon/email.png\" alt=\"\" class=\"icon\"/>\n            <ion-input class=\"ion-padding-start\" placeholder=\"Nombre y apellido\" type=\"text\" formControlName=\"referenceName2\"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row [ngStyle]=\"{'width':'100%'}\">\n        <ion-col>\n          <ion-item class=\"input-round ion-margin-top\">\n            <img src=\"/assets/icon/telefono.png\" alt=\"\" class=\"icon\"/>\n            <ion-input class=\"ion-padding-start\" placeholder=\"Número de teléfono\" type=\"text\" formControlName=\"referenceNumber2\"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row [ngStyle]=\"{'width':'100%'}\">\n        <ion-col>\n          <ion-item class=\"input-round ion-margin-vertical\">\n            <img src=\"/assets/icon/email.png\" alt=\"\" class=\"icon\"/>\n            <ion-input class=\"ion-padding-start\" placeholder=\"Dirección\" type=\"text\" formControlName=\"referenceDirection2\"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row [ngStyle]=\"{'width':'100%'}\">\n        <ion-col>\n          <span class=\"ion-margin-top\">Datos del vehiculo</span>\n        </ion-col>\n      </ion-row>\n\n      <ion-row [ngStyle]=\"{'width':'100%'}\">\n        <ion-col>\n          <ion-item class=\"input-round ion-margin-top\">\n            <img src=\"/assets/icon/email.png\" alt=\"\" class=\"icon\"/>\n            <ion-input class=\"ion-padding-start\" placeholder=\"Número de placa\" type=\"text\" formControlName=\"numberPlate\"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row class=\"ion-align-items-center ion-justify-content-center\">\n\n        <ion-col class=\"ion-float-left\" [ngStyle]=\"{'width':'100%'}\">\n          <div class=\"box-gradient\">\n            <ion-button fill=\"clear\" expand=\"block\" class=\"btn-home\" (click)=\"getPicture('plateFront')\">\n              <span class=\"ion-text-left\">Foto de placa</span>\n              <ion-icon slot=\"end\" name=\"cloud-upload\"></ion-icon>\n            </ion-button>\n          </div>\n        </ion-col>\n\n        <ion-col class=\"ion-float-right\" *ngIf=\"singin3Form.value.plateFront != ''\">\n          <ion-icon name=\"checkmark\"></ion-icon>\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row [ngStyle]=\"{'width':'100%'}\">\n        <ion-col>\n          <ion-item class=\"input-round ion-margin-top\">\n            <img src=\"/assets/icon/email.png\" alt=\"\" class=\"icon\"/>\n            <ion-input class=\"ion-padding-start\" placeholder=\"Número de póliza de seguro\" type=\"text\" formControlName=\"numberPolicy\"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row class=\"ion-align-items-center ion-justify-content-center\">\n\n        <ion-col class=\"ion-float-left\" [ngStyle]=\"{'width':'100%'}\">\n          <div class=\"box-gradient\">\n            <ion-button fill=\"clear\" expand=\"block\" class=\"btn-home\" (click)=\"getPicture('policyFront')\">\n              <span class=\"ion-text-left\">Foto de póliza de seguro</span>\n              <ion-icon slot=\"end\" name=\"cloud-upload\"></ion-icon>\n            </ion-button>\n          </div>\n        </ion-col>\n\n        <ion-col class=\"ion-float-right\" *ngIf=\"singin3Form.value.policyFront != ''\">\n          <ion-icon name=\"checkmark\"></ion-icon>\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row class=\"ion-align-items-center ion-justify-content-center\">\n\n        <ion-col class=\"ion-float-left\" [ngStyle]=\"{'width':'100%'}\">\n          <div class=\"box-gradient\">\n            <ion-button fill=\"clear\" expand=\"block\" class=\"btn-home\" (click)=\"getPicture('identificationCardFront')\" [ngStyle]=\"{'font-size':'0.65rem'}\">\n              <span class=\"ion-text-left\">Foto de tarjeta de identificación frontal</span>\n              <ion-icon slot=\"end\" name=\"cloud-upload\"></ion-icon>\n            </ion-button>\n          </div>\n        </ion-col>\n\n        <ion-col class=\"ion-float-right\" *ngIf=\"singin3Form.value.identificationCardFront != ''\">\n          <ion-icon name=\"checkmark\"></ion-icon>\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row class=\"ion-align-items-center ion-justify-content-center\">\n\n        <ion-col class=\"ion-float-left\" [ngStyle]=\"{'width':'100%'}\">\n          <div class=\"box-gradient\">\n            <ion-button fill=\"clear\" expand=\"block\" class=\"btn-home\" (click)=\"getPicture('identificationCardBack')\" [ngStyle]=\"{'font-size':'0.65rem'}\">\n              <span class=\"ion-text-left\">Foto de tarjeta de identificación posterior</span>\n              <ion-icon slot=\"end\" name=\"cloud-upload\"></ion-icon>\n            </ion-button>\n          </div>\n        </ion-col>\n\n        <ion-col class=\"ion-float-right\" *ngIf=\"singin3Form.value.identificationCardBack != ''\">\n          <ion-icon name=\"checkmark\"></ion-icon>\n        </ion-col>\n\n      </ion-row>\n      \n      <div class=\"ion-margin-top ion-text-center\">\n        <div class=\"box-gradient\">\n          <ion-button expand=\"block\" fill=\"clear\" class=\"btn-home\" type=\"submit\" [disabled]=\"!singin3Form.valid\">Registrarse</ion-button>\n        </div>\n        <span>¿Ya tienes cuenta?<a class=\"link\" [routerLink]=\"['/login']\"> Inicia sesión</a></span>\n      </div>\n\n    </ion-grid>\n\n\n  </form>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/login/signin3/signin3-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/login/signin3/signin3-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: Signin3PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Signin3PageRoutingModule", function() { return Signin3PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _signin3_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./signin3.page */ "./src/app/login/signin3/signin3.page.ts");




const routes = [
    {
        path: '',
        component: _signin3_page__WEBPACK_IMPORTED_MODULE_3__["Signin3Page"]
    }
];
let Signin3PageRoutingModule = class Signin3PageRoutingModule {
};
Signin3PageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], Signin3PageRoutingModule);



/***/ }),

/***/ "./src/app/login/signin3/signin3.module.ts":
/*!*************************************************!*\
  !*** ./src/app/login/signin3/signin3.module.ts ***!
  \*************************************************/
/*! exports provided: Signin3PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Signin3PageModule", function() { return Signin3PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _signin3_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./signin3-routing.module */ "./src/app/login/signin3/signin3-routing.module.ts");
/* harmony import */ var _signin3_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./signin3.page */ "./src/app/login/signin3/signin3.page.ts");







let Signin3PageModule = class Signin3PageModule {
};
Signin3PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _signin3_routing_module__WEBPACK_IMPORTED_MODULE_5__["Signin3PageRoutingModule"]
        ],
        declarations: [_signin3_page__WEBPACK_IMPORTED_MODULE_6__["Signin3Page"]]
    })
], Signin3PageModule);



/***/ }),

/***/ "./src/app/login/signin3/signin3.page.scss":
/*!*************************************************!*\
  !*** ./src/app/login/signin3/signin3.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".background {\n  --background: url(\"/assets/img/bg.png\") 0 0/100% 100% no-repeat;\n}\n\nion-header {\n  position: initial;\n  background: #f6f9fb;\n}\n\nspan.ion-text-left {\n  margin-right: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2pvc2UvUHJvamVjdHMvdGF4aS9jb25kdWN0b3ItdGF4aS9zcmMvYXBwL2xvZ2luL3NpZ25pbjMvc2lnbmluMy5wYWdlLnNjc3MiLCJzcmMvYXBwL2xvZ2luL3NpZ25pbjMvc2lnbmluMy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSwrREFBQTtBQ0NKOztBREVBO0VBQ0ksaUJBQUE7RUFDQSxtQkFBQTtBQ0NKOztBREVBO0VBQ0ksa0JBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL3NpZ25pbjMvc2lnbmluMy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmFja2dyb3VuZHtcbiAgICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1nL2JnLnBuZ1wiKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcbn1cblxuaW9uLWhlYWRlcntcbiAgICBwb3NpdGlvbjogaW5pdGlhbDtcbiAgICBiYWNrZ3JvdW5kOiAjZjZmOWZiO1xufVxuXG5zcGFuLmlvbi10ZXh0LWxlZnQge1xuICAgIG1hcmdpbi1yaWdodDogYXV0bztcbn0iLCIuYmFja2dyb3VuZCB7XG4gIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWcvYmcucG5nXCIpIDAgMC8xMDAlIDEwMCUgbm8tcmVwZWF0O1xufVxuXG5pb24taGVhZGVyIHtcbiAgcG9zaXRpb246IGluaXRpYWw7XG4gIGJhY2tncm91bmQ6ICNmNmY5ZmI7XG59XG5cbnNwYW4uaW9uLXRleHQtbGVmdCB7XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbn0iXX0= */"

/***/ }),

/***/ "./src/app/login/signin3/signin3.page.ts":
/*!***********************************************!*\
  !*** ./src/app/login/signin3/signin3.page.ts ***!
  \***********************************************/
/*! exports provided: Signin3Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Signin3Page", function() { return Signin3Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _services_corek_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/corek.service */ "./src/app/services/corek.service.ts");
/* harmony import */ var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/file-transfer/ngx */ "./node_modules/@ionic-native/file-transfer/ngx/index.js");








let Signin3Page = class Signin3Page {
    constructor(route, router, form, actionSheetController, camera, transfer, _corek, toastController, loadingController) {
        this.route = route;
        this.router = router;
        this.form = form;
        this.actionSheetController = actionSheetController;
        this.camera = camera;
        this.transfer = transfer;
        this._corek = _corek;
        this.toastController = toastController;
        this.loadingController = loadingController;
        // Datos de vista anterior
        this.route.queryParams.subscribe(params => {
            this.signin2Data = this.router.getCurrentNavigation().extras;
        });
        // Formulario 
        this.singin3Form = form.group({
            referenceName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            referenceNumber: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            referenceDirection: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            referenceName2: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            referenceNumber2: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            referenceDirection2: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            numberPlate: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            plateFront: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            numberPolicy: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            policyFront: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            identificationCardFront: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            identificationCardBack: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
        });
    }
    ngOnInit() {
    }
    presentToast(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: message,
                duration: 2000
            });
            toast.present();
        });
    }
    takePicture(sourceType, formValueVariable) {
        const options = {
            quality: 100,
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(options).then((imageData) => {
            this.presentToast('Espere mientras se sube el archivo');
            const namefile = Date.now() + '.jpg';
            let options1 = {
                fileKey: 'file',
                fileName: namefile,
                headers: {}
            };
            const fileTransfer = this.transfer.create();
            fileTransfer.upload(imageData, 'http://157.230.191.152/upload.php', options1).then((data) => {
                this.singin3Form.get(formValueVariable).setValue(namefile);
                this.presentToast('Se subio con exito.');
            }, (err) => {
                alert('Error. Intente de nuevo');
            });
        }, (err) => {
            // Handle error
        });
    }
    getPicture(formValueVariable) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log(formValueVariable);
            const actionSheet = yield this.actionSheetController.create({
                header: 'Foto de pefil',
                buttons: [{
                        text: 'Galeria',
                        icon: 'md-archive',
                        handler: () => {
                            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY, formValueVariable);
                        }
                    }, {
                        text: 'Camara',
                        icon: 'md-camera',
                        handler: () => {
                            this.takePicture(this.camera.PictureSourceType.CAMERA, formValueVariable);
                        }
                    }, {
                        text: 'Cancelar',
                        icon: 'close',
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    }]
            });
            yield actionSheet.present();
        });
    }
    signin3() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                duration: 15000,
                message: 'Espere por favor...',
                translucent: true,
            });
            loading.present();
            let conection = Date.now().toString + 'conection' + Math.random();
            this._corek.ConnectCorekconfig(conection);
            this._corek.socket.on(conection, (response) => {
                let updateUser = Date.now() + 'updateUserSignin3' + Math.random;
                this._corek.socket.emit("update_user", { "set": { "user_status": 1 }, "condition": { "ID": this.signin2Data.id_user }, 'event': updateUser });
                this._corek.socket.on(updateUser, (status) => {
                    let insertMeta = Date.now() + 'insertMetaData' + Math.random();
                    // Cedula
                    this._corek.socket.emit('query', { 'event': insertMeta + 'cedula', 'querystring': "INSERT INTO `wp_usermeta` VALUES ( ''," + this.signin2Data.id_user + ",'cedula','" + this.signin2Data.typeDocument + this.signin2Data.document + "','" + this.signin2Data.identityFron + "','" + this.signin2Data.identityBack + "')" });
                    // Licencia
                    this._corek.socket.emit('query', { 'event': insertMeta + 'licencia', 'querystring': "INSERT INTO `wp_usermeta` VALUES ( ''," + this.signin2Data.id_user + ",'licencia','','" + this.signin2Data.licenseFron + "','" + this.signin2Data.licenseBack + "')" });
                    // Guia
                    this._corek.socket.emit('query', { 'event': insertMeta + 'guia', 'querystring': "INSERT INTO `wp_usermeta` VALUES ( ''," + this.signin2Data.id_user + ",'guia','','" + this.signin2Data.guideFront + "','" + this.signin2Data.guideFront + "')" });
                    // Referencia
                    this._corek.socket.emit('query', { 'event': insertMeta + 'referencia', 'querystring': "INSERT INTO `wp_usermeta` VALUES ( ''," + this.signin2Data.id_user + ",'referencia','" + this.singin3Form.value.referenceName + "','" + this.singin3Form.value.referenceNumber + "','" + this.singin3Form.value.referenceDirection + "')" });
                    // Referencia 2
                    this._corek.socket.emit('query', { 'event': insertMeta + 'referencia2', 'querystring': "INSERT INTO `wp_usermeta` VALUES ( ''," + this.signin2Data.id_user + ",'referencia','" + this.singin3Form.value.referenceName2 + "','" + this.singin3Form.value.referenceNumber2 + "','" + this.singin3Form.value.referenceDirection2 + "')" });
                    // Placa
                    this._corek.socket.emit('query', { 'event': insertMeta + 'placa', 'querystring': "INSERT INTO `wp_usermeta` VALUES ( ''," + this.signin2Data.id_user + ",'placa','" + this.singin3Form.value.numberPlate + "','" + this.singin3Form.value.plateFront + "','')" });
                    // Poliza
                    this._corek.socket.emit('query', { 'event': insertMeta + 'poliza', 'querystring': "INSERT INTO `wp_usermeta` VALUES ( ''," + this.signin2Data.id_user + ",'poliza','" + this.singin3Form.value.numberPolicy + "','" + this.singin3Form.value.policyFront + "','')" });
                    // Identificacion
                    this._corek.socket.emit('query', { 'event': insertMeta + 'identificacion', 'querystring': "INSERT INTO `wp_usermeta` VALUES ( ''," + this.signin2Data.id_user + ",'identificacion','','" + this.singin3Form.value.identificationCardFront + "','" + this.signin2Data.licenseBack + "')" });
                    this._corek.socket.on(insertMeta + 'identificacion', (responses) => {
                        loading.dismiss();
                        this.presentToast('Registro de datos exitoso.');
                        this.router.navigate(['login']);
                    });
                });
            });
        });
    }
};
Signin3Page.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ActionSheetController"] },
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_5__["Camera"] },
    { type: _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_7__["FileTransfer"] },
    { type: _services_corek_service__WEBPACK_IMPORTED_MODULE_6__["CorekService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] }
];
Signin3Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-signin3',
        template: __webpack_require__(/*! raw-loader!./signin3.page.html */ "./node_modules/raw-loader/index.js!./src/app/login/signin3/signin3.page.html"),
        styles: [__webpack_require__(/*! ./signin3.page.scss */ "./src/app/login/signin3/signin3.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ActionSheetController"],
        _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_5__["Camera"],
        _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_7__["FileTransfer"],
        _services_corek_service__WEBPACK_IMPORTED_MODULE_6__["CorekService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]])
], Signin3Page);



/***/ })

}]);
//# sourceMappingURL=login-signin3-signin3-module-es2015.js.map