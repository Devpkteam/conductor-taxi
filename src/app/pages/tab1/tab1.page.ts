import { Component } from '@angular/core';
import { CorekService } from '../../services/corek.service'

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  message = 'No Disponible';
  trips: any[] = [];
  loader:boolean = true;

  constructor(
    private _corek:CorekService
  ) {}

  ionViewWillEnter(){
    let conection = Date.now().toString+'conection'+Math.random();
    this._corek.ConnectCorekconfig(conection);
    this._corek.socket.on(conection, (response)=>{
      this.searchTrips();
    });
  }

  searchTrips(){
    this.trips = [];
    let queryTrips = Date.now().toString+'queryPost'+Math.random();
    this._corek.socket.emit('query_post', { 'condition': {"post_status":0}, 'event':queryTrips});
    this._corek.socket.on(queryTrips, (trips) => {
      let locations = ' '; 
      if(trips.length > 0){
        for (let trip of trips){
          let directions = JSON.parse(trip.post_content)
          for (let direction of directions){
            locations += direction.name.substr(0,40)+' - ';
          }
          this.trips.push({id:trip.ID ,title:trip.post_title, type:trip.post_type, point:locations, time:'6:00 PM'})
        }
        this.loader = false;
      }else{

      }
    });
  }

  reserveTrip(ID){
    let updatePTrips = Date.now().toString+'updatePost'+Math.random();
    this._corek.socket.emit("update_post",{"set":{'post_status':1},"condition":{"ID":ID}, 'event':updatePTrips});
    this._corek.socket.on(updatePTrips, (response) => {
      this.loader = true;
      this.searchTrips();
    });

  }

  changeToggle(event){
    this.message = (event.detail.checked) ? 'Disponible': 'No Disponible'   
  }

}
