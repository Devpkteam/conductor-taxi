import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ActionSheetController, ToastController, LoadingController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { CorekService } from '../../services/corek.service';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';


@Component({
  selector: 'app-signin3',
  templateUrl: './signin3.page.html',
  styleUrls: ['./signin3.page.scss'],
})
export class Signin3Page implements OnInit {

  signin2Data: any;
  singin3Form: FormGroup;

  constructor(private route: ActivatedRoute, 
    private router: Router,
    public form:FormBuilder, 
    public actionSheetController: ActionSheetController, 
    private camera: Camera,
    private transfer: FileTransfer,
    private _corek:CorekService,
    public toastController: ToastController,
    public loadingController: LoadingController,
  ){ 
    // Datos de vista anterior
    this.route.queryParams.subscribe(params => {
      this.signin2Data = this.router.getCurrentNavigation().extras;
    });
    // Formulario 
    this.singin3Form = form.group({
      referenceName:  ['', Validators.required],
      referenceNumber: ['', Validators.required],
      referenceDirection: ['', Validators.required],
      referenceName2:  ['', Validators.required],
      referenceNumber2: ['', Validators.required],
      referenceDirection2: ['', Validators.required],
      numberPlate: ['', Validators.required],
      plateFront: ['', Validators.required],
      numberPolicy: ['', Validators.required],
      policyFront: ['', Validators.required],
      identificationCardFront: ['', Validators.required],
      identificationCardBack: ['', Validators.required],
    });
  }

  ngOnInit() {
  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  takePicture(sourceType, formValueVariable){
    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      this.presentToast('Espere mientras se sube el archivo');
      const namefile =  Date.now()+'.jpg' ; 
      let options1: FileUploadOptions = {
        fileKey: 'file',
        fileName: namefile,
        headers: {}
      }
      const fileTransfer: FileTransferObject = this.transfer.create();
      fileTransfer.upload(imageData, 'http://157.230.191.152/upload.php', options1).then((data) => {
        this.singin3Form.get(formValueVariable).setValue(namefile);
        this.presentToast('Se subio con exito.')
      }, (err) => {
        alert('Error. Intente de nuevo')
      });
    }, (err) => {
      // Handle error
    });
  }

  async getPicture(formValueVariable){
    console.log(formValueVariable)
    const actionSheet = await this.actionSheetController.create({
      header: 'Foto de pefil',
      buttons: [{
        text: 'Galeria',
        icon: 'md-archive',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY, formValueVariable)
        }
      }, {
        text: 'Camara',
        icon: 'md-camera',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.CAMERA, formValueVariable)
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  async signin3(){
    const loading = await this.loadingController.create({
      duration: 15000,
      message: 'Espere por favor...',
      translucent: true,
    });
    loading.present();
    let conection = Date.now().toString+'conection'+Math.random();
    this._corek.ConnectCorekconfig(conection);
    this._corek.socket.on(conection, (response)=>{
      let updateUser = Date.now()+'updateUserSignin3'+Math.random
      this._corek.socket.emit("update_user",{"set":{"user_status":1},"condition":{"ID":this.signin2Data.id_user}, 'event':updateUser});
      this._corek.socket.on(updateUser, (status)=>{
        let insertMeta = Date.now()+'insertMetaData'+Math.random();
        // Cedula
        this._corek.socket.emit('query',{'event':insertMeta+'cedula', 'querystring':"INSERT INTO `wp_usermeta` VALUES ( '',"+this.signin2Data.id_user+",'cedula','"+this.signin2Data.typeDocument+this.signin2Data.document+"','"+this.signin2Data.identityFron+"','"+this.signin2Data.identityBack+"')"});
        // Licencia
        this._corek.socket.emit('query',{'event':insertMeta+'licencia', 'querystring':"INSERT INTO `wp_usermeta` VALUES ( '',"+this.signin2Data.id_user+",'licencia','','"+this.signin2Data.licenseFron+"','"+this.signin2Data.licenseBack+"')"});
        // Guia
        this._corek.socket.emit('query',{'event':insertMeta+'guia', 'querystring':"INSERT INTO `wp_usermeta` VALUES ( '',"+this.signin2Data.id_user+",'guia','','"+this.signin2Data.guideFront+"','"+this.signin2Data.guideFront+"')"});
        // Referencia
        this._corek.socket.emit('query',{'event':insertMeta+'referencia', 'querystring':"INSERT INTO `wp_usermeta` VALUES ( '',"+this.signin2Data.id_user+",'referencia','"+this.singin3Form.value.referenceName+"','"+this.singin3Form.value.referenceNumber+"','"+this.singin3Form.value.referenceDirection+"')"});
        // Referencia 2
        this._corek.socket.emit('query',{'event':insertMeta+'referencia2', 'querystring':"INSERT INTO `wp_usermeta` VALUES ( '',"+this.signin2Data.id_user+",'referencia','"+this.singin3Form.value.referenceName2+"','"+this.singin3Form.value.referenceNumber2+"','"+this.singin3Form.value.referenceDirection2+"')"});
        // Placa
        this._corek.socket.emit('query',{'event':insertMeta+'placa', 'querystring':"INSERT INTO `wp_usermeta` VALUES ( '',"+this.signin2Data.id_user+",'placa','"+this.singin3Form.value.numberPlate+"','"+this.singin3Form.value.plateFront+"','')"});
        // Poliza
        this._corek.socket.emit('query',{'event':insertMeta+'poliza', 'querystring':"INSERT INTO `wp_usermeta` VALUES ( '',"+this.signin2Data.id_user+",'poliza','"+this.singin3Form.value.numberPolicy+"','"+this.singin3Form.value.policyFront+"','')"});
        // Identificacion
        this._corek.socket.emit('query',{'event':insertMeta+'identificacion', 'querystring':"INSERT INTO `wp_usermeta` VALUES ( '',"+this.signin2Data.id_user+",'identificacion','','"+this.singin3Form.value.identificationCardFront+"','"+this.signin2Data.licenseBack+"')"});
        this._corek.socket.on(insertMeta+'identificacion', (responses)=>{
          loading.dismiss();
          this.presentToast('Registro de datos exitoso.');
          this.router.navigate(['login']);
        });
      });
    });
  }

}
