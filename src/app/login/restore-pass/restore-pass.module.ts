import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RestorePassPageRoutingModule } from './restore-pass-routing.module';

import { RestorePassPage } from './restore-pass.page';
import { ModalPage } from '../../pages/modal/modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RestorePassPageRoutingModule
  ],
  declarations: [RestorePassPage, ModalPage],
  entryComponents: [ModalPage]
})
export class RestorePassPageModule {}
