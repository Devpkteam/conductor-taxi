import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ActionSheetController, ToastController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';


@Component({
  selector: 'app-signin2',
  templateUrl: './signin2.page.html',
  styleUrls: ['./signin2.page.scss'],
})
export class Signin2Page implements OnInit {

  idUser:any;
  singin2Form: FormGroup;

  constructor(public form:FormBuilder, 
    public actionSheetController: ActionSheetController, 
    private camera: Camera,
    private route: ActivatedRoute, 
    private router: Router,
    private transfer: FileTransfer,
    public toastController: ToastController,
  ){

    // Datos de vista anterior
    this.route.queryParams.subscribe(params => {
      this.idUser = this.router.getCurrentNavigation().extras;
    });

    this.singin2Form = form.group({
      id_user: [''],
      typeDocument:  ['', Validators.required],
      document: ['', Validators.required],
      identityFron: ['', Validators.required],
      identityBack: ['', Validators.required],
      licenseFron: ['', Validators.required],
      licenseBack: ['', Validators.required],
      lenguages: ['', Validators.required],
      guideFront: ['', Validators.required],
      guideBack: ['', Validators.required],
    });

  }

  ngOnInit() {
  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  takePicture(sourceType, formValueVariable){
    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      this.presentToast('Espere mientras se sube el archivo');
      const namefile =  Date.now()+'.jpg' ; 
      let options1: FileUploadOptions = {
        fileKey: 'file',
        fileName: namefile,
        headers: {}
      }
      const fileTransfer: FileTransferObject = this.transfer.create();
      fileTransfer.upload(imageData, 'http://157.230.191.152/upload.php', options1).then((data) => {
        this.singin2Form.get(formValueVariable).setValue(namefile);
          this.presentToast('Se subio con exito.')
        }, (err) => {
          alert('Error. Intente de nuevo')
      });
    }, (err) => {
      // Handle error
    });
  }

  async getPicture(formValueVariable){
    console.log(formValueVariable)
    const actionSheet = await this.actionSheetController.create({
      header: 'Foto de pefil',
      buttons: [{
        text: 'Galeria',
        icon: 'md-archive',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY, formValueVariable)
        }
      }, {
        text: 'Camara',
        icon: 'md-camera',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.CAMERA, formValueVariable)
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  signin2(){
    this.singin2Form.value.id_user = this.idUser
    this.router.navigate(['signin3'], this.singin2Form.value);
  }

}
